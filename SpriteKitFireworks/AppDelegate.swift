//
//  AppDelegate.swift
//  SpriteKitFireworks
//
//  Created by Tony Chamblee on 8/2/17.
//  Copyright © 2017 Cocoa Niblets. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        return true
    }
}

