//
//  GameViewController.swift
//  SpriteKitFireworks
//
//  Created by Tony Chamblee on 8/2/17.
//  Copyright © 2017 Cocoa Niblets. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

enum GameViewControllerConstants
{
    static let initialFireworkDelay : TimeInterval = 2.0
    static let maxTimeBetweenFireworks : TimeInterval = 1.0
    static let minFireworks = 0
    static let maxFireworks = 4
    static let fireworkChance = 0.3
    static let fireworkInterval : TimeInterval = 0.25
    static let fireworkPositionInsetAmount = 20.0
}

class GameViewController: UIViewController
{
    var lastFireworkTime : TimeInterval = 0.0
    var fireworkTimer : Timer? = nil
    var fireworkEmitterNodes = NSMutableArray()
    var scene : SKScene? = nil
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let view = self.view as! SKView?
        {
            self.scene = SKScene(size: CGSize(width: view.bounds.size.width, height: view.bounds.size.height))
            
            guard let scene = self.scene else
            {
                assertionFailure()
                return
            }
            
            scene.scaleMode = .aspectFill
            
            view.presentScene(scene)
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        lastFireworkTime = Date().timeIntervalSince1970
        perform(#selector(spawnFireworkIfNeeded), with: nil, afterDelay: GameViewControllerConstants.initialFireworkDelay)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        invalidateFireworkTimer()
    }
    
    // MARK: - Overrides

    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    // MARK: - Spawning Fireworks
    
    func invalidateFireworkTimer()
    {
        if (fireworkTimer != nil)
        {
            fireworkTimer?.invalidate()
            fireworkTimer = nil
        }
    }
    
    func spawnFireworkIfNeeded()
    {
        var shouldSpawnFirework = false
        let currentTime = Date().timeIntervalSince1970
        let diff = currentTime - lastFireworkTime
        
        if (diff >= GameViewControllerConstants.maxTimeBetweenFireworks)
        {
            shouldSpawnFirework = true
        }
        else if (fireworkEmitterNodes.count < GameViewControllerConstants.minFireworks)
        {
            shouldSpawnFirework = true
        }
        else if (fireworkEmitterNodes.count  > GameViewControllerConstants.maxFireworks)
        {
            shouldSpawnFirework = false
        }
        else
        {
            shouldSpawnFirework = randomChanceWithPercentage(GameViewControllerConstants.fireworkChance)
        }
        
        if (shouldSpawnFirework)
        {
            lastFireworkTime = currentTime
            
            let emitterPosition = randomFireworkPosition()
            
            addOuterEmitter(at: emitterPosition, color: randomFireworkColor())
            addInnerEmitter(at: emitterPosition)
            
            // TODO: adding a nice firework sound with a little variance in pitch here has a nice effect
        }
        
        fireworkTimer = Timer.scheduledTimer(timeInterval: GameViewControllerConstants.fireworkInterval, target: self, selector: #selector(spawnFireworkIfNeeded), userInfo: nil, repeats: false)
    }
    
    func addOuterEmitter(at position: CGPoint, color: UIColor)
    {
        guard let emitter = SKEmitterNode(fileNamed: "FireworkExplosion"), let scene = self.scene else
        {
            assertionFailure()
            return
        }
        
        emitter.position = position
        emitter.particleColor = color
        
        scene.addChild(emitter)
        
        let waitAction = SKAction.wait(forDuration: TimeInterval(emitter.particleLifetime))
        let removeAction = SKAction.run({
            emitter.removeFromParent()
            self.fireworkEmitterNodes.remove(emitter)
        })
        let sequence = SKAction.sequence([waitAction, removeAction])
        
        emitter.run(sequence)
        fireworkEmitterNodes.add(emitter)
    }
    
    func addInnerEmitter(at position: CGPoint)
    {
        guard let emitter = SKEmitterNode(fileNamed: "FireworkInner"), let scene = self.scene else
        {
            assertionFailure()
            return
        }
        
        emitter.position = position
        
        scene.addChild(emitter)
        
        let waitAction = SKAction.wait(forDuration: TimeInterval(emitter.particleLifetime))
        let removeAction = SKAction.removeFromParent()
        let sequence = SKAction.sequence([waitAction, removeAction])
        
        emitter.run(sequence)
    }
    
    func randomFireworkPosition() -> CGPoint
    {
        guard let scene = self.scene else
        {
            assertionFailure()
            return .zero
        }
        
        let insetAmount = GameViewControllerConstants.fireworkPositionInsetAmount
        let x = arc4random() % UInt32((Double(scene.size.width) - insetAmount) + (insetAmount / 2.0))
        let y = arc4random() % UInt32((Double(scene.size.height) - insetAmount) + (insetAmount / 2.0))
        
        return CGPoint(x: Int(x), y: Int(y))
    }
    
    func randomFireworkColor() -> UIColor
    {
        let colorIndex = arc4random() % 7
        
        switch colorIndex
        {
            case 0:
                return .red
            case 1:
                return .green
            case 2:
                return .blue
            case 3:
                return .yellow
            case 4:
                return .purple
            case 5:
                return .orange
            default:
                return .white
        }
    }
    
    func randomChanceWithPercentage(_ percentage: Double) -> Bool
    {
        assert(percentage >= 0)
        assert(percentage <= 1)
        
        let amount = 10000.0
        let valueToBeat = amount * percentage
        let random = arc4random() % UInt32(amount) + 1
        
        return (Double(random) <= valueToBeat)
    }
    
}
